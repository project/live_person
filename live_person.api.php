<?php

/**
 * @file
 * Hook documentation for the LivePerson module
 */


/**
 * Allows modules to alter the visibility result for embedding LiveChat on the
 * current page.
 *
 * @param $visibility
 *   The default value determined by the role and page based visibility settings.
 */
function hook_live_person_visibility_alter(&$visibility) {
  // No example.
}
