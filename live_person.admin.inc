<?php

/**
 * @file
 * Administrative page callbacks for the LivePerson module.
 */


/**
 * Builds the settings form for the LivePerson module.
 */
function live_person_admin_settings_form($form, &$form_state) {
  $form['account']['live_person_account'] = array(
    '#type' => 'textfield',
    '#title' => t('LivePerson account number'),
    '#description' => live_person_account_number() == '' ? t("Don't have an account yet? <a href=\"!url\" target=\"_blank\">Register here</a> to get started with a special promotion for Drupal users.", array('!url' => live_person_registration_url())) : '',
    '#default_value' => live_person_account_number(),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
  );

  // Allow users to restrict LiveChat by page path.
  $form['page_visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page based LiveChat visibility'),
  );
  $form['page_visibility']['live_person_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Restrict the LiveChat widget to specific pages'),
    '#options' => array(
      0 => t('Add LiveChat to every page except the pages listed below.'),
      1 => t('Add LiveChat to only the pages listed below.'),
    ),
    '#default_value' => variable_get('live_person_visibility', 0),
  );
  $form['page_visibility']['live_person_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('live_person_pages', ''),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#wysiwyg' => FALSE,
  );

  // Build an options list of roles to exclude from LiveChat.
  $options = array();

  foreach (user_roles() as $rid => $name) {
    $options[$rid] = check_plain($name);
  }

  $form['role_visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role based LiveChat visibility'),
  );
  $form['role_visibility']['live_person_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Do not show the LiveChat widget to users with any of these roles'),
    '#default_value' => variable_get('live_person_roles', array()),
    '#options' => $options,
    '#description' => t('If no roles are selected, all users will see the LiveChat widget.'),
  );

  return system_settings_form($form);
}
